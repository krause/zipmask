function zipmask_test()

d = 10;
m = rand(d, d, d);
zs = zeros(d, d, d);

    function test_cube()
        mask = zs;
        l = ceil(d/4);
        mask(1:l, 2:l+1, 3:l+1) = 1;
        s = zipmask_pack(m, mask);
        [um, umask] = zipmask_unpack(s);
        assert(isequal(umask, mask));
        assert(isequal(um, m .* mask));
    end

    function test_plane()
        mask = zs;
        mask(:, :, 1) = 1;
        s = zipmask_pack(m, mask);
        [um, umask] = zipmask_unpack(s);
        assert(isequal(umask, mask));
        assert(isequal(um, m .* mask));
    end

    function test_sphere()
        mask = zs;
        c = ceil(d/2);
        for x = 1:d
          for y = 1:d
            for z = 1:d
                if (x - c)^2 + (y - c)^2 + (z - c)^2 <= 4
                    mask(x, y, z) = 1;
                end
            end
          end
        end
        s = zipmask_pack(m, mask);
        [um, umask] = zipmask_unpack(s);
        assert(isequal(umask, mask));
        assert(isequal(um, m .* mask));
        clear x y z;
    end

    function test_scalar()
        mask = zs;
        p = randi([1,d], [1,3]);
        mask(p(1), p(2), p(3)) = 1;
        s = zipmask_pack(m, mask);
        [um, umask] = zipmask_unpack(s);
        assert(isequal(umask, mask));
        assert(isequal(um, m .* mask));
    end

    function test_empty()
        mask = zs;
        s = zipmask_pack(m, mask);
        [um, umask] = zipmask_unpack(s);
        assert(isequal(umask, mask));
        assert(isequal(um, m .* mask));
    end

test_cube()
test_plane()
test_sphere()
test_scalar()
test_empty()

end
