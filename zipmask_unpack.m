function [matrix, mask] = zipmask_unpack(s);
%
%  [X, Y] = zipmask_unpack(S);
%
%  Blows up a structure with a compressed matrix/roi into its original size.
%
%  Use zipmask_pack(X, Y) to pack/reduce a matrix based on a mask.

matrix = zeros(s.dimension);
mask = matrix;

x0 = s.position(1);
y0 = s.position(2);
z0 = s.position(3);
[xl, yl, zl] = size(s.data);

matrix(x0:x0+xl-1, y0:y0+yl-1, z0:z0+zl-1) = s.data;
mask(x0:x0+xl-1, y0:y0+yl-1, z0:z0+zl-1) = s.mask;

end
