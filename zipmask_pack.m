function out = zipmask_pack(matrix, mask);
%
%  struct = zipmask_pack(X, Y);
%
%  Apply a binary mask Y on a matrix X and reduces the matrix dimensions to
%  a simple cuboid hull. The function returns a strcuture containing the
%  smaller matrices for matrix and mask and references to the original size and
%  its position in the original space.
%
%  Use zipmask_unpack(struct) to grow the matrix back into its original size.
%  Note that this is not the inverse operation to pack.
%
%  Only works with 3D data right now.
%

d_mat = size(matrix);
d_mask = size(mask);

if ( length(d_mat) ~= length(d_mask) || ...
     any(d_mat ~= d_mask) )
    d_mat
    d_mask
    error('Matrix and ROI mask dimensions have to match');
end

if length(d_mat) ~= 3
    error('Only implemented for 3D arrays, yet');
end

if numel(mask) ~= numel(find(mask==0)) + numel(find(mask==1))
    error('Mask appears to be non-binary');
end

if all(all(all(mask == 0)))
    warning('Mask is empty');
    out.data = [];
    out.mask = [];
    out.dimension = size(matrix);
    out.position = [1, 1, 1];
    out.ratio = 0;
    return;
end

% get cuboid around mask
tmpvec = find(sum(squeeze(sum(mask, 3)), 2) ~= 0);
mins(1) = tmpvec(1);
tmpvec = find(sum(squeeze(sum(mask, 3)), 1) ~= 0);
mins(2) = tmpvec(1);
tmpvec = find(sum(squeeze(sum(mask, 2)), 1) ~= 0);
mins(3) = tmpvec(1);

tmpvec = find(sum(squeeze(sum(mask, 3)), 2) ~= 0);
maxs(1) = tmpvec(end);
tmpvec = find(sum(squeeze(sum(mask, 3)), 1) ~= 0);
maxs(2) = tmpvec(end);
tmpvec = find(sum(squeeze(sum(mask, 2)), 1) ~= 0);
maxs(3) = tmpvec(end);

% apply mask
matrix = matrix .* mask;

% set outputs
out.data = matrix(mins(1):maxs(1), mins(2):maxs(2), mins(3):maxs(3));
out.mask = mask(mins(1):maxs(1), mins(2):maxs(2), mins(3):maxs(3));
out.position = [mins(1), mins(2), mins(3)];
out.dimension = size(matrix);
out.ratio = prod(size(out.data)) / prod(size(matrix));

end
