# What's this?

This is a very simple pair of functions `pack` and `unpack` that accept a 3D
matrix and a binary mask of the same dimension. It will "compress" the matrix
by cutting away everything the is not part of a cuboid hull of the mask and
saves the original dimension and position in a structure.

It's intended to be used in larger arrays of arrays that contain masked brain
data (hence 3D) to reduce the memory footprint.

# Example

(see [doc/zipmask_example.m](doc/zipmask_example.m) )

First, we create a 20x20x20 cube and a spherical mask in the center:

```matlab
d = 20;
m = rand(d, d, d);

mask = zeros(d, d, d);
c = ceil(d/2);
for x = 1:d
  for y = 1:d
    for z = 1:d
        if (x - c)^2 + (y - c)^2 + (z - c)^2 <= 16
            mask(x, y, z) = 1;
        end
    end
  end
end

plot_lattice3D(m, mask, 'original matrix/mesh');
```
![plot1](doc/plot1.png)

We cut out everything outside of the mask with `zipmask_pack()`:

```matlab
s = zipmask_pack(m, mask);
plot_lattice3D(s.data, s.mask, 'compressed matrix/mesh');
```
![plot1](doc/plot2.png)


And re-grow the small data with `zipmask_unpack()`:

```matlab
[um, umask] = zipmask_unpack(s);
plot_lattice3D(um, umask, 'expanded matrix/mesh');
```
![plot1](doc/plot3.png)


# Limitations

Only works in 3D for now.
