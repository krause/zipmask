function zipmask_example()

    function plot_lattice3D(matrix, mask, t);
        figure;
        l = size(matrix, 1); % expect cube
        k = 1;
        j = 1;
        for x = 1:l
            for y = 1:l
                for z = 1:l
                    if mask(x, y, z)
                        mask_xs(j) = x;
                        mask_ys(j) = y;
                        mask_zs(j) = z;
                        j = j + 1;
                    else
                        matrix_xs(k) = x;
                        matrix_ys(k) = y;
                        matrix_zs(k) = z;
                        k = k + 1;
                    end
                end
            end
        end

        scatter3(mask_xs, mask_ys, mask_zs, 10, 'filled', '-bo');
        hold on;
        scatter3(matrix_xs, matrix_ys, matrix_zs, 1, '-ro');
        axis equal;
        axis square;
        title(t);
    end

d = 20;
m = rand(d, d, d);

% create spherecial mask around the center of the cube with r=4
mask = zeros(d, d, d);
c = ceil(d/2);
for x = 1:d
  for y = 1:d
    for z = 1:d
        if (x - c)**2 + (y - c)**2 + (z - c)**2 <= 16
            mask(x, y, z) = 1;
        end
    end
  end
end

plot_lattice3D(m, mask, 'original matrix/mesh');

% mask and pack the data
s = zipmask_pack(m, mask);
plot_lattice3D(s.data, s.mask, 'compressed matrix/mesh');

% unpack data
[um, umask] = zipmask_unpack(s);
plot_lattice3D(um, umask, 'expanded matrix/mesh');

end
